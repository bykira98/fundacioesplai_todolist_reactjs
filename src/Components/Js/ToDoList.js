import React, { Component } from 'react'

import { Container, Row, Col, Label, Navbar, NavbarBrand  } from 'reactstrap';
import Formulari from "./Formulari";
import Llista from "./Lista";


import "../Css/ToDoList.css";

export default class ToDoList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            tasques: []
        }

        this.novaTasca = this.novaTasca.bind(this);
        this.esborraTasca = this.esborraTasca.bind(this);
    }

    esborraTasca(tasca) {

        let novaLlista = this.state.tasques.filter(todo => todo.id !== tasca.id);
        this.setState({
            tasques: novaLlista
        });

    }

    novaTasca(text, type) {
        // this.state hauria de ser inmutable
        // si fem un "push" el modifiquem, amb "concat" es torna un nou array
        //  cites:  this.state.cites.concat({dia, tema})
        // la sintaxi següent és alternativa ES6
        var id = this.state.tasques.length + 1;
        this.setState({
            tasques: [...this.state.tasques, { id: id, text: text, type: type }]
        });
    }

    render() {
        return (
            <>
                <Navbar color="dark">
                    <NavbarBrand >
                        Todolist
                    </NavbarBrand>
                </Navbar >

                <div className="espai50"></div>
                <Container>
                    <Row>
                        <Col md="6">
                            <Formulari novaTasca={this.novaTasca} tasques={this.state.tasques} />
                        </Col>
                        <Col md="6" className="lineleft">
                            <Label>Tasques</Label>
                            <Llista esborra={this.esborraTasca} tasques={this.state.tasques} />
                        </Col>
                    </Row>
                </Container>
            </>
        )
    }
}