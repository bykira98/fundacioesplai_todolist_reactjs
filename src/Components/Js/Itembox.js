import React, { Component } from 'react'

import { Button, Alert } from 'reactstrap';

export default class Itembox extends Component {
    render() {
        return (
            <Alert color={this.props.tipo}>
                {this.props.tasca.text}
                <Button close aria-label="Cancel" onClick={() => this.props.esborra(this.props.tasca)}>
                    <span aria-hidden="true">&times;</span>
                </Button>
            </Alert>
        );
    }
}