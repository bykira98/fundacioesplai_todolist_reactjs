import React, { Component } from 'react'
import { Button, Form, FormGroup, Input, Label } from 'reactstrap';
export default class Formulari extends Component {
    constructor(props) {
        super(props);
        this.state = {
            tasca: '', tipo: ''
        }
        this.enviaForm = this.enviaForm.bind(this);
        this.canviTasca = this.canviTasca.bind(this);
        this.canviTarea = this.canviTarea.bind(this);
    }

    canviTasca(event) {
        this.setState({ tasca: event.target.value });
    }

    canviTarea(event)
    {
        this.setState({ tipo: event.target.value });
    }

    enviaForm(event) {
        //console.log(event);
        this.props.novaTasca(this.state.tasca, this.state.tipo);
        this.setState({
            tasca: '', tipo: ''
        })
        event.preventDefault();
    }

    render() {
        let i = 1;
        return (
            <Form onSubmit={this.enviaForm}>
                <FormGroup>
                    <Label>Tasca</Label>
                    <Input type="text" value={this.state.tasca} className="form-control" onChange={this.canviTasca} />
                </FormGroup>
                <FormGroup>
                    <Label>Tipo de Tarea</Label>
                    <Input type="select" name="TipoTarea" id="TipoTarea" value={this.state.tipo} onChange={this.canviTarea}>
                        <option>primary</option>
                        <option>success</option>
                        <option>danger</option>
                        <option>warning</option>
                    </Input>
                </FormGroup>
                <Button type="submit" className="btn-primary">Enviar</Button>
            </Form>
        );
    }
}