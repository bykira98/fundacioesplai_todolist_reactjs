import React, { Component } from 'react'

import Itembox from "./Itembox";

export default class Lista extends Component {
    render() {
        let i = 1;
        
        let tasques = this.props.tasques.map(todo => <Itembox key={todo.id} esborra={this.props.esborra} tasca={todo} tipo={todo.type}/>)
        return (
            <div>
                {tasques}
            </div>
        );
    }
}